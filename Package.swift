// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "plantinject",
    platforms: [
//        .linux(),
        .macOS(.v12)
    ],
    dependencies: [
        .package(url: "https://github.com/yeokm1/SwiftSerial.git", from: "0.1.2"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-SQLite.git", from: "5.0.0"),
        .package(url: "https://github.com/apple/swift-argument-parser", .upToNextMajor(from: "1.0.0"))
    ],
    targets: [
        .executableTarget(name: "plantinject", dependencies: ["PlantInjectLib"]),
        .target(
            name: "PlantInjectLib",
            dependencies: [
                .product(name: "SwiftSerial", package: "SwiftSerial"),
                .product(name: "PerfectSQLite", package: "Perfect-SQLite"),
                .product(name: "ArgumentParser", package: "swift-argument-parser")

            ],
            resources: [
                .copy("plantinject.entitlements")]
	     ),
        .testTarget(
            name: "plantinjectTests",
            dependencies: ["plantinject"])
    ]
)
