#
# Makefile for plantinject
#

install: plantinject

plantinject: Package.swift Sources/PlantInjectLib/* Sources/plantinject/*
	swift build -c release
	cp .build/release/plantinject .
