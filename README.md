# Plant Inject
A command line tool to inject values transmitted over the serial port to an SQLite3 database.

## Usage
First set up your database file in main's 
`sqliteDbPath`. It defaults to `readings.sqlite3`, because, well, that's what I use.

You probably want to change the fields you are injecting in the `SQLiteHandler`.

The value from the serial port is extracted using the ORS Packet Descriptor in the delegate method `serialPortWasOpened` in the `SerialHandler`. 
Currently the serial protocol format is 

`[1345]
`
where 1345 is the value to extract.

This value is then injected using the `sqliteInjector`, which is set up in main.

The application is started with 
`plantinject <serialport device>`

e.g.: `plantinject /dev/cu.usbmodem14101`

Some people found that using the `cu` version of the port works better than the `tty` version.

