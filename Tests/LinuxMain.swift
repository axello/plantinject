import XCTest

import plantinjectTests

var tests = [XCTestCaseEntry]()
tests += plantinjectTests.allTests()
XCTMain(tests)
