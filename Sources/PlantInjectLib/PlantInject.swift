
import ArgumentParser
import Foundation

let sqliteDbPath: String = "readings.sqlite3"   //"path to sqlite3 database"
var db: SQLiteHandler!
var serialHandler: SerialHandler!

enum PlantError: Error {
    case normalExit
    case serialPortNotInitialised
    case databaseProblem
}

public struct PlantInject: ParsableCommand {
    @Argument(help: "The serial port device") 
    var serialPort: String
    
    public static let version = "0.0.2"

    public static let configuration = CommandConfiguration(
        commandName: "plantinject",
        abstract: "Listen to the serial port and insert into sqlite3 database.",
        version: Self.version)

    func sqliteInjector(_ string: String) {
        if let intValue = Int(string) {
            db.write(sensorId: 1, sensorValue: Double(intValue))
        }
        print("value: \(string)")
    }

    func sqliteInjector(data: Data) {
        let valueString = String(data:data, encoding:.utf8)!
        if let intValue = Int(valueString) {
            db.write(sensorId: 1, sensorValue: Double(intValue))
        }
        print("value: \(valueString)")
    }

    static func getSerialPortArgument() -> String? {
        let serialPort = CommandLine.arguments[1]
        return serialPort
    }

    public init() {}

    public func run() throws {

        do {
            db = try SQLiteHandler(databaseFile: sqliteDbPath)
        } catch {
            print("could not open sqlite3 database '\(sqliteDbPath)'")
            throw(PlantError.databaseProblem)
        }

        guard let serialPortHandler = SerialHandler(device: serialPort, injector: sqliteInjector) else {
            throw(PlantError.serialPortNotInitialised)
        }
        print("Hello, serial device: \(serialPort)")

        serialHandler = serialPortHandler
        serialHandler.waitOnSerial()

        // When backgrounding daemon style, use & to keep script in background
        RunLoop.main.run()
    }
}
