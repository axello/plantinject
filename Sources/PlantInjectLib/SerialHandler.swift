//
//  SerialHandler.swift
//  plantinject
//
//  Created by Axel Roest on 22-02-2020.
//  Copyright © 2020-2022 Phluxux. All rights reserved.
//
// Update: use https://github.com/yeokm1/SwiftSerial/blob/master/Sources/SwiftSerial.swift, as that is 100% swift

import Foundation
import SwiftSerial

typealias InjectDataHandler = (String) -> (Void)
var port: SerialPort?

class SerialHandler: NSObject {
    private let dataInjector: InjectDataHandler
    private var receiving: Bool = false
    private let startTag: Character = "["
    private let endTag: Character = "]"
    
    init?(device: String, injector: @escaping InjectDataHandler) {
        dataInjector = injector
        super.init()
        if let newPort = openPort(device) {
            port = newPort
            receiving = true
        } else {
            print("Serial device \'\(device)\' not found.")
            return nil
        }
    }
    
    func openPort(_ device: String) -> SerialPort? {
        let port = SerialPort(path: device)
        // baudrates other than 9600 give a buggy response
        port.setSettings(receiveRate: .baud9600, transmitRate: .baud9600, minimumBytesToRead: 3)
        do {
            try port.openPort()
        } catch {
            print("Could not open serial device \'\(device)\'.")
            return nil
        }
        return port
    }
    
    deinit {
        if let port = port {
            port.closePort()
            print("Port Closed")
        }
    }
    
    func waitOnSerial() {
    #if os(Linux)
        linuxBackgroundRead()

    #elseif os(OSX)
        if let port = port {
            self.backgroundRead(port: port, dataHandler: self.dataInjector)
        }
    #endif
    }
    
    func linuxBackgroundRead() {
        guard let port = port else { return }
        
        var line = ""
        while true {
            do{
                let data = try port.readData(ofLength: 3)
                if let text = String(data: data, encoding: .ascii) {
                    line.append(text)
                }
                let (value, rest) = processLine(line)
                if let value = value {
                    dataInjector(value)
                }
                line = rest
            } catch {
                print("Error: \(error)")
            }
        }
    }

    func backgroundRead(port: SerialPort, dataHandler: @escaping InjectDataHandler) {
        var line = ""
        while true {
//            sleep(1)
            do{
                let data = try port.readData(ofLength: 3)
                if let text = String(data: data, encoding: .ascii) {
                    line.append(text)
                }
                let (value, rest) = processLine(line)
                if let value = value {
                    dataHandler(value)
                }
                line = rest
            } catch {
                print("Error: \(error)")
            }
        }
    }
    
    func processLine(_ line: String) -> (String?, String) {
        if let tagIndex = line.firstIndex(of: startTag), tagIndex < line.endIndex {
            let startOfSentence = line.index(after: tagIndex)
            let firstSentence = line[startOfSentence...]

            if let endTagIndex = firstSentence.firstIndex(of: endTag) {
                let endOfValue = firstSentence.index(before: endTagIndex)
                let value = firstSentence[...endOfValue]
                let afterEndTagIndex = firstSentence.index(after: endTagIndex)
                let restString = firstSentence[afterEndTagIndex...]
                return (String(value), String(restString))
            }
        }
        return (nil, line)
    }
}
