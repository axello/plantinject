//
//  SQLiteHandler.swift
//  plantinject
//
//  Created by Axel Roest on 22-02-2020.
//  Copyright © 2020-2022 Phluxux. All rights reserved.
//
// On linux you have to do
// sudo apt-get install libsqlite3-dev

import Foundation
import PerfectSQLite

enum SQLiteError: Error {
    case openingDBFailure
    case creatingTableFailure
}

class SQLiteHandler {
    var dbPath: String
    let dateFormatter = DateFormatter()
    
    init(databaseFile: String) throws {
        var url = URL(fileURLWithPath: databaseFile)
        if "sqlite3" != url.pathExtension {
            url.appendPathExtension("sqlite3")
        }
        print("Readings database at: \(url.path)")
        dbPath = url.path
        
        let sqlite = try SQLite(dbPath)
        defer {
            sqlite.close()
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"            // compatible with sqlite3 injection
        try setup()
    }
    
    func createTable() throws {
        let query = "CREATE TABLE IF NOT EXISTS readings(id INTEGER PRIMARY KEY AUTOINCREMENT,sensor INTEGER, date REAL, value REAL);"
        
        do {
            let sqlite = try SQLite(dbPath)
            defer {
                sqlite.close()
            }

            try sqlite.execute(statement: query)
            print("Reading table creation success")
        } catch (let error) {
            print("Failure creating database tables: \(error)") //Handle Errors
        }
    }
    
    func setup() throws {
        try createTable()
    }
    
    func write(sensorId: Int, sensorValue: Double) {
        let query = "INSERT INTO readings (sensor, date, value) VALUES (:1, :2, :3);"
        let now = Date().timeIntervalSince1970 // dateFormatter.string(from: Date())

        do {
            let sqlite = try SQLite(dbPath)
            defer {
                sqlite.close()
            }

            try sqlite.execute(statement: query) {
                (stmt:SQLiteStmt) -> () in
                
                try stmt.bind(position: 1, sensorId)
                try stmt.bind(position: 2, now)
                try stmt.bind(position: 3, sensorValue)
            }
        } catch (let error) {
            print("error inserting reading: \(error)")
        }
    }
}
